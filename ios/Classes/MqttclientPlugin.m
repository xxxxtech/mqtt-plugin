#import "MqttclientPlugin.h"
#import <mqttclient/mqttclient-Swift.h>

@implementation MqttclientPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftMqttclientPlugin registerWithRegistrar:registrar];
}
@end
